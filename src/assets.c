#include "init.h"

sig_atomic_t got_cont = 0; /* Set if we received the SIGCONT signal */
sig_atomic_t got_signals; /* Set if we received a signal. */

/*
 *	We got a signal (HUP PWR WINCH ALRM INT)
 */
void signal_handler(int sig) {
	ADDSET(got_signals, sig);
}
