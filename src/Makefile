#!/usr/bin/make -f
MAKE:=make
CC:=gcc
CFLAGS:=-std=c11 -Os -Wall -D_GNU_SOURCE -DWITH_SELINUX
LINKFLAGS:=-lsepol -lselinux

CYTHON:=cython3
CYTHON_CFLAGS:=-I/usr/include/python3.5m -fdata-sections -ffunction-sections
#CYTHON_LINKFLAGS:=-lpython3.5m -lpthread -lm -lutil -ldl # simple opts
CYTHON_LINKFLAGS:=-Wl,-Bstatic -lpython3.5m -Wl,-Bdynamic -Wl,--gc-sections -Wl,--print-gc-sections -lpthread -lm -lutil -ldl -lz -lexpat #cython statically, all other dynamically

MAKEDEP:=$(CC) $(CFLAGS) -M

TARGETS:=$(patsubst %,../target/%,init halt poweroff reboot runlevel shutdown telinit update-sysmakeinit)

SOURCES:=assets.c halt.c init.c poweroff.c reboot.c runlevel.c shutdown.c telinit.c
DEPS:=$(patsubst %.c,../target/%.dep,$(SOURCES))

OBJS:=assets.o init.o halt.o poweroff.o runlevel.o reboot.o shutdown.o telinit.o update-sysmakeinit.o update-sysmakeinit.c

.PHONY: default
default:
	@cat TARGETS.txt

.PHONY: all
all: $(TARGETS)
	@echo Done.

-include $(DEPS)

.PHONY: dep
.NOTPARALLEL: dep
dep:
	$(MAKE) -f Makefile.dep realdep

../target/.stamp:
	[ -d ../target ] || mkdir -pv ../target
	touch ../target/.stamp

../target/%.o: %.c .%.dep ../target/.stamp
	$(CC) $(CFLAGS) -c -o $@ $<

../target/init: ../target/init.o assets.o
	$(CC) -o $@ init.o assets.o $(LINKFLAGS)

../target/halt: ../target/halt.o
	$(CC) -o $@ $< $(LINKFLAGS)

../target/poweroff: ../target/poweroff.o
	$(CC) -o $@ $< $(LINKFLAGS)

../target/runlevel: ../target/runlevel.o
	$(CC) -o $@ $< $(LINKFLAGS)

../target/reboot: ../target/reboot.o
	$(CC) -o $@ $< $(LINKFLAGS)

../target/shutdown: ../target/shutdown.o
	$(CC) -o $@ $< $(LINKFLAGS)

../target/telinit: ../target/telinit.o ../target/assets.o
	$(CC) -o $@ ../target/telinit.o ../target/assets.o $(LINKFLAGS)

../target/update-sysmakeinit.c: update_sysmakeinit.pyx ../target/.stamp
	$(CYTHON) --embed -o $@ $<

../target/update-sysmakeinit.o: ../target/update-sysmakeinit.c
	$(CC) $(CFLAGS) $(CYTHON_CFLAGS) -c -o $@ $<

../target/update-sysmakeinit: ../target/update-sysmakeinit.o
	$(CC) -o $@ $< $(CYTHON_LINKFLAGS) $(LINKFLAGS)

.PHONY: clean
clean:
	rm -f $(OBJS) $(TARGETS)

.PHONY: distclean
distclean: clean
	rm -f $(DEPS)
