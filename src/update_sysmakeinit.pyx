#!/usr/bin/python3 -f

import os
import re
import pprint
import sys
import argparse

## GLOBAL DEFS

etc = '/etc'

INITBEGIN="### BEGIN INIT INFO"
INITEND="### END INIT INFO"
DEFAULT_OUTPUT="/var/lib/sysmakeinit/Makefile"
DISABLED_SERVICES=["mountkernfs.sh", "mountdevsubfs.sh", "networking", "syslog-ng", "time", "named", "plymouth", "udev", "procps", "thin", "sysstat", "killprocs"]

## GLOBAL STATS

services = {}
insservLines = []
makeTargets = {}
virtualFacilities = {}
providedFacilities = {}

logLevel = 1

## ASSETS

def debug(string):
  if logLevel >= 3:
    sys.stderr.write(string + "\n")

def warning(string):
  if logLevel >= 1:
    sys.stderr.write(string + "\n")

def error(string):
  if logLevel >= 1:
    sys.stderr.write(string + "\n")
  sys.exit(1)

class SmiException(Exception):
  def __init__(self, message):
    super(Exception, self).__init__(message)

def is_exe(path):
  return os.path.isfile(path) and os.access(path, os.X_OK)

def trim(string):
  return string.rstrip('\r\n \t')

def trimBoth(string):
  string = re.sub(r'^[\t ]*', '', string)
  string = re.sub(r'[\t ]*$', '', string)
  return string

def cleanServiceList(services):
  ret = []
  for service in services:
    if service[0:1] == "$":
      service = service[1:]
    elif service[0:1] == "+":
      service = service[1:]
      if not Service.exist(service):
        continue
    ret.append(service)
  return ret

def checkIfServiceExist(name):
  if not name in services:
    warning("refered service of virtual facility " + name + " does not exist")
    return False
  else:
    return True

def assertServiceExist(name):
  if not name in services:
    raise SmiException("refered service of virtual facility " + name + " does not exist")

## MAIN CLASSES

class Service:
  global services

  name = None

  def __init__(self, name = None):
    debug("new Service, name: " + name)
    if name:
      self.setName(name)
    self.deps = []

  def get(name):
    assertServiceExist(name)
    return services[name]

  def getName(self):
    if not self.name:
      raise SmiException("Service has no name yet")
    return self.name

  def setName(self, name):
    debug("Service setName: " + str(self.name) + "->" + name)
    #debug("services type: " + str(type(services)))
    #debug("services: " + str(services))

    if self.name == name:
      return
    if self.name:
      raise SmiException("service name can be set once");
    self.name = name
    if name in services:
      raise SmiException("service " + name + " already defined\n" + repr(services.keys()));
    services[self.name] = self

  def exists(name):
    return name in services

class InitScript(Service):
  def __init__(self):
    self.config = {}
    self.startMakeTarget = None
    self.stopMakeTarget = None
    self.providedFacilities = []

  def setConfigOpt(self, name, value):
    #print ("self.config: " + pprint.pprint(self.config))
    if name in self.config:
      error("service configuration option " + name + " declared multiple times" + pprint.pprint(self.config))
    self.config[name] = value.split()

  def getStartMakeTarget(self):
    if not self.startMakeTarget:
      raise SmiException
    return self.startMakeTarget

  def getStopMakeTarget(self):
    if not self.stopMakeTarget:
      raise SmiException
    return self.stopMakeTarget

  def addProvidedFacility(self, facility):
    self.providedFacilities.append(facility)

  def process(self):
    self.startMakeTarget = MakeTarget(self.name + "-start", self)
    self.startMakeTarget.addRecipe("/etc/init.d/" + self.name + " status || exec /etc/init.d/" + self.name + " start")

    self.stopMakeTarget = MakeTarget(self.name + "-stop", self)
    self.stopMakeTarget.addRecipe("exec /etc/init.d/" + self.name + " stop")

  def removeOptionalDeps(self):
    # none, happens per-line in the processDeps phase
    True

  def processDeps(self):
    for name, value in self.config.items():
      methodName = InitScript.optionToMethodName(name)
      try:
        method = getattr(self, methodName)
        method(value)
      except AttributeError:
        error('service ' + self.name + ': method ' + methodName + ' not implemented: '+repr(self))

  def processConfigProvides(self, args):
    raise SmiException("unreachable code")

  def processConfigShortDescription(self, args):
    True

  def processConfigDescription(self, args):
    True

  def processConfigRequiredStart(self, args):
    args = cleanServiceList(args)
    for serviceName in args:
      if not checkIfServiceExist(serviceName):
        continue
      else:
        self.startMakeTarget.addDep(Service.get(serviceName).getStartMakeTarget())

  def processConfigRequiredStop(self, args):
    args = cleanServiceList(args)
    for serviceName in args:
      if not checkIfServiceExist(serviceName):
        continue
      else:
        Service.get(serviceName).getStopMakeTarget().addDep(self.stopMakeTarget)

  def processConfigShouldStart(self, args):
    args = cleanServiceList(args)
    for serviceName in args:
      if not serviceName in services:
        continue
      self.startMakeTarget.addDep(Service.get(serviceName).getStartMakeTarget())

  def processConfigShouldStop(self, args):
    args = cleanServiceList(args)
    for serviceName in args:
      if not serviceName in services:
        continue
      Service.get(serviceName).getStopMakeTarget().addDep(self.stopMakeTarget)

  def processConfigDefaultStart(self, args):
    for runlevelName in args:
      Runlevel.get(runlevelName).getMakeTarget().addDep(self.startMakeTarget)

  def processConfigDefaultStop(self, runlevels):
    for runlevelName in runlevels:
      Runlevel.get(runlevelName).getMakeTarget().addDep(self.stopMakeTarget)

  def processConfigXStartBefore(self, args):
    args = cleanServiceList(args)
    for serviceName in args:
      if not serviceName in services:
        continue
      Service.get(serviceName).getStartMakeTarget().addDep(self.startMakeTarget)

  def processConfigXStopAfter(self, args):
    args = cleanServiceList(args)
    for serviceName in args:
      if not serviceName in services:
        continue
      self.stopMakeTarget.addDep(Service.get(serviceName).getStopMakeTarget())

  def processConfigXInteractive(self, args):
    if len(args) != 1:
      warning("X-Interactive allows only a single argument")
    value = args[0]
    if value.lower() == "true":
      self.startMakeTarget.addFlag("NOTPARALLEL")
      self.stopMakeTarget.addFlag("NOTPARALLEL")

  # converts "Required-Start" to "processRequiredStart"
  def optionToMethodName(string):
    toks = string.split('-')
    outToks = []
    for tok in toks:
      outToks.append(tok[0:1].upper() + tok[1:].lower())
    return 'processConfig' + ''.join(outToks)

  def load(path):
    with open(path) as f:
      content = f.readlines()

    content = list(map(trim, content))

    try:
      bi = content.index(INITBEGIN)
      ei = content.index(INITEND)
      if (ei < bi):
        raise ValueError
    except ValueError:
      warning(path + " does not have correct BEGIN/END INIT INFO lines")
      return

    initScript = InitScript()
    initScript.setName(os.path.basename(path))

    content = content[bi+1:ei]
    for line in content:
      if re.match(r'^[#\t ]{3}', line):
        continue
      line = re.sub(r'^[#\t ]*', '', line)       
      c = line.count(':')
      if c == 0:
        continue;
      elif c>1:
        warning("invalid config line in " + path + ": " + line)

      c = line.split(':')
      optName = trimBoth(c[0])
      optValue = trimBoth(c[1])

      if optName == "Provides":
        debug("Provides: " + optValue)
        for name in optValue.split():
          if name == initScript.getName():
            continue
          else:
            providedFacility = ProvidedFacility(name, initScript)
      else:
        initScript.setConfigOpt(optName, optValue)

class VirtualFacility(Service):
  global virtualFacilities

  def __init__(self, name):
    debug("new VirtualFacility " + name)
    super(VirtualFacility, self).__init__(name)
    self.startMakeTarget = None
    self.stopMakeTarget = None
    self.aliases = []
    virtualFacilities[name] = self

  def addAlias(self, alias):
    if alias[0:1] == "$":
      alias = alias[1:]
    if not alias in self.aliases:
      self.aliases.append(alias)

  def get(name):
    #debug("VirtualFacility.get: " + name + " virtualFacilities: " + repr(virtualFacilities.keys()))
    #pprint.pprint(virtualFacilities)
    if not name in virtualFacilities:
      return VirtualFacility(name)
    else:
      return virtualFacilities[name]

  def getAliases(self):
    return self.aliases

  def getStartMakeTarget(self):
    if not self.startMakeTarget:
      raise SmiException
    return self.startMakeTarget

  def getStopMakeTarget(self):
    if not self.stopMakeTarget:
      raise SmiException
    return self.stopMakeTarget

  def process(self):
    if self.name != "<interactive>":
      self.startMakeTarget = MakeTarget(self.name + "-start", self)
      self.stopMakeTarget = MakeTarget(self.name + "-stop", self)

  def removeOptionalDeps(self):
    newAliases = []
    for alias in self.aliases:
      if alias[0:1] == "+":
        alias = alias[1:]
        if not Service.exists(alias):
          continue
      newAliases.append(alias)
    self.aliases = newAliases

  def processDeps(self):
    for alias in self.aliases:
      if not checkIfServiceExist(alias):
        continue
      if self.name == "<interactive>":
        Service.get(alias).getStartMakeTarget().addFlag("NOTPARALLEL")
        Service.get(alias).getStopMakeTarget().addFlag("NOTPARALLEL")
      else:
        self.startMakeTarget.addDep(services[alias].getStartMakeTarget())
        self.stopMakeTarget.addDep(services[alias].getStopMakeTarget())

  def processInsservLine(toks):
    vfacName = toks[0]
    vfacAliases = toks[1:]

    if vfacName[0] == "$":
      vfacName = vfacName[1:]
    elif vfacName == "<interactive>":
      True
    else:
      raise SmiException("virtual facility config for not virtual facility: " + " ".join(toks));
    
    vfac = VirtualFacility.get(vfacName)
    for vfacAlias in vfacAliases:
      vfac.addAlias(vfacAlias)

  def loadInsservConf(path):
    with open(path) as f:
      content = f.readlines()

    content = list(map(trim, content))
    for line in content:
      if line.count('#'):
        line = re.sub(r'#.*$', '', line)
      line = trimBoth(line)
      toks = line.split()
      if not toks:
        continue
      debug("loadInsservConf: " + ",".join(toks))
      VirtualFacility.processInsservLine(toks)

class Runlevel(Service):
  def __init__(self, name):
    assert len(name) == 1
    super(Runlevel, self).__init__(name)
    self.start = []
    self.stop = []
    self.makeTarget = None

  def starts(self, service):
    if not service in self.start:
      self.start.append(service)

  def stops(self, service):
    if not service in self.stop:
      self.stop.append(service)

  def getMakeTarget(self):
    if not self.makeTarget:
      self.makeTarget = MakeTarget(self.name, self)
    return self.makeTarget

  def process(self):
    self.getMakeTarget()

  def removeOptionalDeps(self):
    True

  def processDeps(self):
    for service in self.start:
      self.makeTarget.addDep(service.getStartMakeTarget())
    for service in self.stop:
      self.makeTarget.addDep(service.getStopMakeTarget())

  def get(name):
    assert len(name) == 1
    if not Service.exists(name):
      return Runlevel(name)
    else:
      return Service.get(name)

class ProvidedFacility(Service):
  global providedFacilities

  def __init__(self, name, providedBy):
    super(ProvidedFacility, self).__init__(name)
    self.providedBy = providedBy
    providedFacilities[name] = self
    providedBy.addProvidedFacility(self)
    
  def getStartMakeTarget(self):
    if not self.startMakeTarget:
      raise SmiException
    return self.startMakeTarget

  def getStopMakeTarget(self):
    if not self.stopMakeTarget:
      raise SmiException
    return self.stopMakeTarget

  def process(self):
    self.startMakeTarget = MakeTarget(self.name + "-start", self)
    self.stopMakeTarget = MakeTarget(self.name + "-stop", self)

  def removeOptionalDeps(self):
    True

  def processDeps(self):
    self.startMakeTarget.addDep(self.providedBy.getStartMakeTarget())
    self.stopMakeTarget.addDep(self.providedBy.getStopMakeTarget())

class MakeTarget:
  def __init__(self, name, service):
    if name in makeTargets:
      raise SmiException('target ' + name + ' defined multiple times')
    self.name = name
    self.recipe = []
    self.deps = []
    self.flags = ["PHONY"]
    makeTargets[name] = self
    self.recDeps = False
    self.service = service

  def addFlag(self, name):
    if not name in self.flags:
      self.flags.append(name)

  def addDep(self, target):
    if self.name == target.name:
      warning("self-dependency ignored: " + self.name)
      return
    if not target in self.deps:
      self.deps.append(target)

  def addRecipe(self, command):
    self.recipe.append(command)

  def dump(self):
    global DISABLED_SERVICES

    if self.service.name in DISABLED_SERVICES:
      return

    if self.flags:
      for flag in self.flags:
        print ("." + flag + ": " + self.name)
    if self.deps:
      debug("deps of " + self.name + ": " + ",".join(i.name for i in self.deps))
      list.sort(self.deps)
      depsNoDisabled = []
      for dep in self.deps:
        if not dep.service.name in DISABLED_SERVICES:
          depsNoDisabled.append(dep)
      print (self.name + ": " + ' '.join(dep.name for dep in depsNoDisabled))
    else:
      print (self.name + ":")

    if self.recipe:
      self.recipe.insert(0, "@echo Starting " + self.name)
      self.recipe.append("@echo " + self.name + " ready.")
    else:
      self.recipe.append("@echo " + self.name + " done.")

    if (self.recipe):
      print ("\t" + "\n\t".join(self.recipe)+"\n")
    else:
      print ();

  def getRecDepsIn(self, forbidden):
    recDeps = set()
    newForbidden = list(forbidden)
    newForbidden.append(self)
    for dep in self.deps:
      if dep in recDeps:
        continue
      if dep in forbidden:
        warning("circular dependency detected: " + self.name + " in " + ",".join(i.name for i in forbidden))
        continue
      recDeps.add(dep)
      recDeps = recDeps.union(dep.getRecDepsIn(newForbidden))
    return recDeps

  def getRecDeps(self):
    if self.recDeps == False:
      debug("getRecDeps: " + self.name)
      self.recDeps = self.getRecDepsIn([])
      debug("result: " + " ".join(i.name for i in self.recDeps))
    return self.recDeps

  def __hash__(self):
    return self.name.__hash__()

  def order(self):
    if len(self.name) > 1:
      return 0
    elif self.name == "S":
      return 1
    else:
      return 2

  def __lt__(self, x):
    if self.name == x.name:
      return False

    ord1 = self.order()
    ord2 = x.order()
    if ord1 != ord2:
      return ord1 < ord2

    if ord1 > 0:
      return self.name < x.name

    if self in x.getRecDeps():
      if x in self.getRecDeps():
        warning("mutual dependency detected: " + self.name + " vs. " + x.name)
        return self.name < x.name
      else:
        return True
    else:
      if x in self.getRecDeps():
        return False
      else:
        return self.name < x.name

  def __gt__(self, x):
    if self.name == x.name:
      return False
    else:
      return not self.__lt__(x)

  def __eq__(self, x):
    return self.name == x.name

## MAIN

def prepare():
  global logLevel

  parser = argparse.ArgumentParser(description = "Updates sysmakeinit Makefile")
  parser.add_argument('--debug', action='store_const', const='debug', help="Debug modus")
  parser.add_argument('--warning', action='store_const', const='warning', help="Show warnings")
  parser.add_argument('--quiet', action='store_const', const='quiet', help="Says nothing")
  parser.add_argument('--output', help='Generated makefile output, "-" for stdout', default=DEFAULT_OUTPUT, nargs=1)

  args = parser.parse_args()
  
  if args.quiet == 'quiet':
    logLevel = 0

  if args.warning == 'warning':
    logLevel = 2

  if args.debug == 'debug':
    logLevel = 3

  for vfac in ["local_fs", "network", "named", "portmap", "remote_fs", "syslog", "time", "all"]:
    VirtualFacility(vfac)

def load():
  initd = os.path.abspath(etc + '/init.d')
  for file in os.listdir(initd):
    path = initd + '/' + file
    debug ("path=" + path)
    if not is_exe(path):
      debug(path + " not executable regular file, ignored")
    else:
      InitScript.load(path)

  insservconf = os.path.abspath(etc + '/insserv.conf')
  VirtualFacility.loadInsservConf(insservconf)

  insservconfd = os.path.abspath(etc + '/insserv.conf.d')
  for file in os.listdir(insservconfd):
    VirtualFacility.loadInsservConf(insservconfd + '/' + file)

def process():
  for name, service in services.items():
    service.process()

def removeOptionalDeps():
  for name, service in services.items():
    service.removeOptionalDeps()

def processDeps():
  for name in list(services.keys()):
    service = services[name]
    service.processDeps()

def dump():
  print ("#!/usr/bin/make -f")
  targetList = list(makeTargets.values())
  list.sort(targetList)
  for target in targetList:
    target.dump()

prepare()
load()
process()
removeOptionalDeps()
processDeps()
dump()
