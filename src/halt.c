/*
 * Halt		Stop the system running.
 *		It re-enables CTRL-ALT-DEL, so that a hard reboot can
 *		be done. If called as reboot, it will reboot the system.
 *
 *		If the system is not in runlevel 0 or 6, halt will just
 *		execute a "shutdown -h" to halt the system, and reboot will
 *		execute an "shutdown -r". This is for compatibility with
 *		sysvinit 2.4.
 *
 * Usage:	halt [-n] [-w] [-d] [-f] [-h] [-i] [-p]
 *		-n: don't sync before halting the system
 *		-f: force halt/reboot, don't call shutdown.
 *		-p: power down the system (if possible, otherwise halt).
 *
 *		Reboot and halt are both this program. Reboot
 *		is just a link to halt. Invoking the program
 *		as poweroff implies the -p option.
 *
 * Author:	Miquel van Smoorenburg, miquels@cistron.nl
 *
 * Version:	2.86,  30-Jul-2004
 *
 *		This file is part of the sysvinit suite,
 *		Copyright (C) 1991-2004 Miquel van Smoorenburg.
 *
 *		This program is free software; you can redistribute it and/or modify
 *		it under the terms of the GNU General Public License as published by
 *		the Free Software Foundation; either version 2 of the License, or
 *		(at your option) any later version.
 *
 *		This program is distributed in the hope that it will be useful,
 *		but WITHOUT ANY WARRANTY; without even the implied warranty of
 *		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *		GNU General Public License for more details.
 *
 *		You should have received a copy of the GNU General Public License
 *		along with this program; if not, write to the Free Software
 *		Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/times.h>
#include <time.h>
#include <signal.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include "reboot.h"

char *Version = "@(#)halt  2.86  31-Jul-2004 miquels@cistron.nl";
char *progname;

#define KERNEL_MONITOR	1 /* If halt() puts you into the kernel monitor. */
#define RUNLVL_PICKY	0 /* Be picky about the runlevel */

/*
 *	Send usage message.
 */
void usage(void) {
	fprintf(stderr, "usage: %s [-n] [-w] [-d] [-f] [-h] [-i]%s\n", progname,
			strcmp(progname, "halt") ? "" : " [-p]");
	fprintf(stderr, "\t-n: don't sync before halting the system\n");
	fprintf(stderr, "\t-f: force halt/reboot, don't call shutdown.\n");
	if (!strcmp(progname, "halt"))
		fprintf(stderr,
				"\t-p: power down the system (if possible, otherwise halt).\n");
	exit(1);
}

void warn(char* str) {
	int saved_errno = errno;
	fprintf(stderr, "WARN: %s (%s, %i)\n", str, strerror(saved_errno),
			saved_errno);
}

/*
 *	Switch to another runlevel.
 */
void do_shutdown(char *fl, char *tm) {
	char *args[8];
	int i = 0;

	args[i++] = "shutdown";
	args[i++] = fl;
	if (tm) {
		args[i++] = "-t";
		args[i++] = tm;
	}
	args[i++] = "now";
	args[i++] = NULL;

	execv("/sbin/shutdown", args);
	execv("/etc/shutdown", args);
	execv("/bin/shutdown", args);

	perror("shutdown");
	exit(1);
}

/*
 *	Main program.
 *	Reboot or halt.
 */
int main(int argc, char **argv) {
	int do_reboot = 0;
	int do_sync = 1;
	int do_nothing = 0;
	int do_hard = 0;
	int do_poweroff = 0;
	int c;
	char *tm = NULL;

	/*
	 *	Find out who we are
	 */
	/* Remove dash passed on in argv[0] when used as login shell. */
	if (argv[0][0] == '-')
		argv[0]++;
	if ((progname = strrchr(argv[0], '/')) != NULL)
		progname++;
	else
		progname = argv[0];

	if (!strcmp(progname, "reboot"))
		do_reboot = 1;
	if (!strcmp(progname, "poweroff"))
		do_poweroff = 1;

	/*
	 *	Get flags
	 */
	while ((c = getopt(argc, argv, ":fnpt:")) != EOF) {
		switch (c) {
		case 'n':
			do_sync = 0;
			break;
		case 'f':
			do_hard = 1;
			break;
		case 'p':
			do_poweroff = 1;
			break;
		case 't':
			tm = optarg;
			break;
		default:
			usage();
		}
	}
	if (argc != optind)
		usage();

	if (geteuid() != 0) {
		fprintf(stderr, "%s: must be superuser.\n", progname);
		exit(1);
	}

	if (chdir("/") < 0)
		warn("can not change to /");

	if (!do_hard && !do_nothing) {
		do_shutdown(do_reboot ? "-r" : "-h", tm);
	}

	/*
	 *	Exit if all didn't want to do any more.
	 */
	if (do_nothing)
		exit(0);

	if (do_sync) {
		sync();
		sleep(2);
	}

	if (do_nothing)
		exit(0);

	if (do_reboot) {
		init_reboot(BMAGIC_REBOOT);
	} else {
		/*
		 *	Turn on hard reboot, CTRL-ALT-DEL will reboot now
		 */
#ifdef BMAGIC_HARD
		init_reboot(BMAGIC_HARD);
#endif

		/*
		 *	Stop init; it is insensitive to the signals sent
		 *	by the kernel.
		 */
		kill(1, SIGTSTP);

		/*
		 *	Halt or poweroff.
		 */
		if (do_poweroff)
			init_reboot(BMAGIC_POWEROFF);
		/*
		 *	Fallthrough if failed.
		 */
		init_reboot(BMAGIC_HALT);
	}

	/*
	 *	If we return, we (c)ontinued from the kernel monitor.
	 */
#ifdef BMAGIC_SOFT
	init_reboot(BMAGIC_SOFT);
#endif
	kill(1, SIGCONT);

	exit(0);
}
