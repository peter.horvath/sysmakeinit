#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "init.h"
#include "initreq.h"

/*
 * General info/warning/error print
 */
void print(char* s) {
	fprintf(stderr, "%s\n", s);
}

/*
 * General errno warning
 */
void warn(char* s) {
	int saved_errno = errno;
	char buf[4096];
	snprintf(buf, 4095, "Warning: %s (%i, %s)\n", s, saved_errno,
			strerror(saved_errno));
	print(buf);
}

/*
 * Tell the user about the syntax we expect.
 */
static
void usage(char *s) {
	fprintf(stderr,
			"Usage: %s {-e VAR[=VAL] | [-t SECONDS] {0|1|2|3|4|5|6|S|s|Q|q|A|a|B|b|C|c|U|u}}\n",
			s);
	exit(1);
}

int main(int argc, char **argv) {
#ifdef TELINIT_USES_INITLVL
	FILE *fp;
#endif
	struct init_request request;
	struct sigaction sa;
	int f, fd, l;
	char *env = NULL;

	memset(&request, 0, sizeof(request));
	request.magic = INIT_MAGIC;

	while ((f = getopt(argc, argv, "t:e:")) != EOF)
		switch (f) {
		case 'e':
			if (env == NULL)
				env = request.data;
			l = strlen(optarg);
			if (env + l + 2 > request.data + sizeof(request.data)) {
				fprintf(stderr, "%s: -e option data "
						"too large\n", argv[0]);
				exit(1);
			}
			memcpy(env, optarg, l);
			env += l;
			*env++ = 0;
			break;
		default:
			usage(argv[0]);
			break;
		}

	if (env)
		*env++ = 0;

	if (env) {
		if (argc != optind)
			usage(argv[0]);
		request.cmd = INIT_CMD_SETENV;
	} else {
		if (argc - optind != 1 || strlen(argv[optind]) != 1)
			usage(argv[0]);
		if (!strchr("0123456789SsQqAaBbCcUu", argv[optind][0]))
			usage(argv[0]);
		request.cmd = INIT_CMD_RUNLVL;
		request.runlevel = env ? 0 : argv[optind][0];
	}

	/* Change to the root directory. */
	if (chdir("/") == -1) {
		warn("chdir to / failed (#1)");
	}

	/* Open the fifo and write a command. */
	/* Make sure we don't hang on opening /run/initctl */
	SETSIG(sa, SIGALRM, signal_handler, 0);
	alarm(3);
	if ((fd = open(INIT_FIFO, O_WRONLY)) >= 0) {
		ssize_t p = 0;
		size_t s = sizeof(request);
		void *ptr = &request;

		while (s > 0) {
			p = write(fd, ptr, s);
			if (p < 0) {
				if (errno == EINTR || errno == EAGAIN)
					continue;
				break;
			}
			ptr += p;
			s -= p;
		}
		close(fd);
		alarm(0);
		return 0;
	}

#ifdef TELINIT_USES_INITLVL
	if (request.cmd == INIT_CMD_RUNLVL) {
		/* Fallthrough to the old method. */

		/* Now write the new runlevel. */
		if ((fp = fopen(INITLVL, "w")) == NULL) {
			fprintf(stderr, "%s: cannot create %s\n",
					progname, INITLVL);
			exit(1);
		}
		fprintf(fp, "%s %d", argv[optind], SLTIME);
		fclose(fp);

		/* And tell init about the pending runlevel change. */
		if (kill(INITPID, SIGHUP) < 0) perror(progname);

		return 0;
	}
#endif

	fprintf(stderr, "%s: ", argv[0]);
	if (ISMEMBER(got_signals, SIGALRM)) {
		fprintf(stderr, "timeout opening/writing control channel %s\n",
				INIT_FIFO);
	} else {
		perror(INIT_FIFO);
	}
	return 1;
}
