#!/usr/bin/make -f

PREFIX:=/
INSTALL_USER:=root
INSTALL_GROUP:=root

MANDIR:=$(PREFIX)/usr/share/man
BIN:=$(PREFIX)/bin
DOC:=$(PREFIX)/share/doc/sysmakeinit
ETC:=$(PREFIX)/etc
SBIN:=$(PREFIX)/sbin
USRSBIN:=$(PREFIX)/usr/sbin

INSTALL_DIR:=install -m 755 -d -D
INSTALL_EXEC:=install -m 755 -s -D
INSTALL_DATA:=install -m 644 -D

ifneq ($(INSTALL_USER),)
INSTALL_DIR=$(INSTALL_DIR) -o $(INSTALL_USER)
INSTALL_EXEC=$(INSTALL_EXEC) -o $(INSTALL_USER)
INSTALL_DATA=$(INSTALL_DATA) -o $(INSTALL_USER)
endif

ifneq ($(INSTALL_GROUP),)
INSTALL_DIR=$(INSTALL_DIR) -g $(INSTALL_GROUP)
INSTALL_EXEC=$(INSTALL_EXEC) -g $(INSTALL_GROUP)
INSTALL_DATA=$(INSTALL_DATA) -g $(INSTALL_GROUP)
endif

SBIN:=init halt poweroff shutdown runlevel reboot telinit
USRSBIN:=update-sysmakeinit
MAN5:=halt.5 initscript.5 inittab.5 rcS.5 tmpfs.5
MAN8:=fsck.nfs.8 halt.8 init.8 poweroff.8 reboot.8 runlevel.8 shutdown.8 telinit.8

.PHONY: default
default:
	@cat TARGETS.txt

.PHONY: compile
compile:
	$(MAKE) -C src -f Makefile dep
	$(MAKE) -C src -f Makefile all

.PHONY: clean
clean:
	$(MAKE) -C src -f Makefile clean

.PHONY: distclean
distclean:
	$(MAKE) -C src -f Makefile distclean

.PHONY: all
all: distclean compile install

.PHONY: deb
deb:
	debian/rules clean
	debian/rules build
	fakeroot debian/rules binary
	@#If you don't have a reprepro repo, comment this out

.PHONY: install
install:
	$(INSTALL_DIR) $(PREFIX)/sbin
	cd src && $(INSTALL_EXEC) $(SBIN) -t $(PREFIX)/sbin
	$(INSTALL_DIR) $(PREFIX)/usr/sbin
	cd src && $(INSTALL_EXEC) $(USRSBIN) -t $(PREFIX)/usr/sbin
	$(INSTALL_EXEC) src/update-sysmakeinit $(PREFIX)/usr/sbin
	$(INSTALL_DIR) $(PREFIX)/usr/share/man/man5
	cd man && $(INSTALL_DATA) $(MAN5) -t $(PREFIX)/usr/share/man/man5
	$(INSTALL_DIR) $(PREFIX)/usr/share/man/man8
	cd man && $(INSTALL_DATA) $(MAN8) -t $(PREFIX)/usr/share/man/man8
	$(INSTALL_DIR) $(PREFIX)/etc
	if [ -f etc/inittab.$$MACHTYPE ]; then \
		$(INSTALL_DATA) etc/inittab.$$MACHTYPE $(PREFIX)/etc; \
	else \
		$(INSTALL_DATA) etc/inittab $(PREFIX)/etc; \
	fi
	$(INSTALL_DIR) $(PREFIX)/etc/default
	$(INSTALL_DATA) etc/default/* $(PREFIX)/etc/default
	$(INSTALL_DIR) $(PREFIX)/etc/init.d
	cp -vfa etc/init.d/* $(PREFIX)/etc/init.d
	$(INSTALL_DIR) $(PREFIX)/usr/share/doc/sysmakeinit
	$(INSTALL_DATA) doc/* $(PREFIX)/usr/share/doc/sysmakeinit
	$(INSTALL_DIR) $(PREFIX)/lib/init
	$(INSTALL_DATA) lib/init/* $(PREFIX)/lib/init
	# pipe is recreated only if we install into root
ifeq ($(PREFIX),)
	@if [ ! -p $(PREFIX)/run/initctl ]; then \
		echo "Creating $(PREFIX)/run/initctl"; \
		rm -f $(PREFIX)/run/initctl; \
		mknod -m 600 $(PREFIX)/run/initctl p; fi
endif
