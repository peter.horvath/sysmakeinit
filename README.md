# sysmakeinit 2.99-alpha
Quick &amp; simple, systemd-compatible, maximally parallel init replacement using GNU Make

contrib		Unofficial stuff
doc		Documentation, mostly obsolete
man		Manual pages, not obsolete
src		Source code

The project home is on https://github.com/HorvathAkosPeter/sysmakeinit
